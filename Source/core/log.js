/*
 * @project: TERA
 * @version: Development (beta)
 * @license: MIT (not for evil)
 * @copyright: Yuriy Ivanov (Vtools) 2017-2019 [progr76@gmail.com]
 * Web: https://terafoundation.org
 * Twitter: https://twitter.com/terafoundation
 * Telegram:  https://t.me/terafoundation
*/

require("./constant.js");
var fs = require("fs");
require("./log-strict.js");
var file_name_info = GetDataPath("info.log"), file_name_infoPrev = GetDataPath("info-prev.log");
CheckSizeLogFile(file_name_info, file_name_infoPrev);
var file_name_log = GetDataPath("log.log"), file_name_logPrev = GetDataPath("log-prev.log");
CheckSizeLogFile(file_name_log, file_name_logPrev);
var file_name_log_web = GetDataPath("web.log"), file_name_log_webPrev = GetDataPath("web-prev.log");
CheckSizeLogFile(file_name_log_web, file_name_log_webPrev);
var StartStatTime, file_name_error = GetDataPath("err.log"), file_name_errorPrev = GetDataPath("err-prev.log");

function ToLogFile(e,t,r)
{
    t instanceof Error && (t = t.message + "\n" + t.stack), global.START_SERVER || (t = global.PROCESS_NAME + ": " + t), "MAIN" !== global.PROCESS_NAME && process.send ? process.send({cmd:"log",
        message:t}) : (console.log(START_PORT_NUMBER + ": " + GetStrOnlyTime() + ": " + t), r || SaveToLogFileSync(e, t));
};

function ToLogClient(e,t,r)
{
    e && (ToLogFile(file_name_log, e), t || (t = ""), ArrLogClient.push({text:GetStrOnlyTime() + " " + e, key:t, final:r}), 13 < ArrLogClient.length && ArrLogClient.shift());
};
CheckSizeLogFile(file_name_error, file_name_errorPrev), global.ToLog = function (e,t)
{
    void 0 === t && (t = 1), t && t > global.LOG_LEVEL || (global.ALL_LOG_TO_CLIENT ? ToLogClient(e, void 0, void 0) : ToLogFile(file_name_log,
    e));
}, global.ToLogWeb = function (e)
{
}, global.SmallAddr = function (e)
{
    return e.substr(0, 5);
}, global.ToErrorTrace = function (e)
{
    ToError(e + ":" + (new Error).stack);
}, global.ToLogTrace = function (e)
{
    ToErrorTrace(e);
}, global.ToInfo = function (e)
{
    ToLogFile(file_name_info, e, 1);
}, global.ToError = function (e)
{
    ToLogFile(file_name_error, e);
}, global.ArrLogClient = [], global.ToLogClient = ToLogClient, global.ToLogClient0 = ToLogClient;
var CONTEXT_STATS = {Total:{}, Interval:[]}, CONTEXT_ERRORS = {Total:{}, Interval:[]}, CurStatIndex = 0;

function GetCurrentStatIndex()
{
    var e = 2 * MAX_STAT_PERIOD + 2;
    return CurStatIndex % e;
};

function ResizeArrMax(e)
{
    for(var t = [], r = Math.trunc(e.length / 2), o = 0; o < r; o++)
        t[o] = Math.max(e[2 * o], e[2 * o + 1]);
    return t;
};

function ResizeArrAvg(e)
{
    for(var t = [], r = Math.trunc(e.length / 2), o = 0; o < r; o++)
        t[o] = (e[2 * o] + e[2 * o + 1]) / 2;
    return t;
};

function ResizeArr(e)
{
    for(var t = [], r = Math.trunc(e.length / 2), o = 0; o < r; o++)
        t[o] = e[2 * o];
    return t;
};

function GetDiagramData(e,t)
{
    var r, o = 2 * MAX_STAT_PERIOD + 2;
    r = "MAX:" === t.substr(0, 4);
    for(var n, a = MAX_STAT_PERIOD, l = (GetCurrentStatIndex() - a + o) % o, i = (e.Total, []), T = void 0, g = l; g < l + a; g++)
    {
        var S = g % o;
        if(n = e.Interval[S])
        {
            var _ = n[t];
            void 0 !== _ ? r ? i.push(_) : (void 0 !== T ? i.push(_ - T) : i.push(_), T = _) : i.push(0);
        }
    }
    return i;
};

function CalcInterval(e,t,r)
{
    for(var o, n = 2 * MAX_STAT_PERIOD + 2, a = {}, l = (t - r + n) % n, i = e.Total, T = l; T < l + r; T++)
    {
        var g = T % n;
        if(o = e.Interval[g])
            break;
    }
    if(o)
        for(var S in i)
            "MAX:" === S.substr(0, 4) ? a[S] = 0 : void 0 === o[S] ? a[S] = i[S] : a[S] = i[S] - o[S];
    return a;
};

function AddToStatContext(e,t,r)
{
    void 0 === r && (r = 1);
    var o = e.Total[t];
    o || (o = 0), "MAX:" === t.substr(0, 4) ? o = Math.max(o, r) : o += r, e.Total[t] = o, StartStatTime || (StartStatTime = GetCurrentTime(0));
};

function CopyStatInterval(e,t)
{
    var r = e.Interval[t];
    r || (r = {}, e.Interval[t] = r);
    var o = e.Total;
    for(var n in o)
        r[n] = o[n], "MAX:" === n.substr(0, 4) && (o[n] = 0);
};

function SaveToLogFileAsync(e,o)
{
    fs.open(e, "a", void 0, function (e,r)
    {
        if(e)
            console.log("Ошибка открытия лог-файла ошибок");
        else
        {
            var t = GetStrTime() + " : " + o + "\r\n";
            fs.write(r, t, null, "utf8", function (e,t)
            {
                e ? console.log("Ошибка записи в лог-файл ошибок!") : fs.close(r, function (e)
                {
                    e && console.log(e);
                });
            });
        }
    });
};

function SaveToLogFileSync(e,t)
{
    try
    {
        var r = GetStrTime() + " : " + t + "\r\n", o = fs.openSync(e, "a");
        fs.writeSync(o, r, null, "utf8"), fs.closeSync(o);
    }
    catch(e)
    {
        console.log(e.message);
    }
};
global.PrepareStatEverySecond = function ()
{
    CurStatIndex++;
    var e = GetCurrentStatIndex();
    CopyStatInterval(CONTEXT_STATS, e), CopyStatInterval(CONTEXT_ERRORS, e);
}, global.TO_ERROR_LOG = function (e,t,r,o,n,a)
{
    r instanceof Error && (r = r.message + "\n"), "rinfo" === o ? r += " from: " + n.address + ":" + n.port : "node" === o && (r += " from: " + n.ip + ":" + n.port);
    var l = e + ":" + t;
    ToError(" ==ERROR== " + l + " " + r), AddToStatContext(CONTEXT_ERRORS, l), ADD_TO_STAT("ERRORS");
}, global.HASH_RATE = 0, global.ADD_HASH_RATE = function (e)
{
    e /= 1e6, global.HASH_RATE += e, ADD_TO_STAT("HASHRATE", e);
}, global.GET_STAT = function (e)
{
    var t = CONTEXT_STATS.Total[e];
    return t || (t = 0), t;
}, global.ADD_TO_STAT_TIME = function (e,t,r)
{
    if(global.STAT_MODE)
    {
        if(r && 2 !== global.STAT_MODE)
            return ;
        var o = process.hrtime(t), n = 1e3 * o[0] + o[1] / 1e6;
        ADD_TO_STAT(e, n);
    }
}, global.ADD_TO_STAT = function (e,t,r)
{
    if(global.STAT_MODE)
    {
        if(r && 2 !== global.STAT_MODE)
            return ;
        AddToStatContext(CONTEXT_STATS, e, t);
    }
}, global.GET_STATDIAGRAMS = function (e)
{
    GetCurrentTime();
    var t = GetCurrentStatIndex();
    if(!e || !e.length)
        return [];
    for(var r = [], o = 0; o < e.length; o++)
    {
        var n = e[o], a = GetDiagramData(CONTEXT_STATS, n);
        r.push({name:n, maxindex:t, arr:a, starttime:StartStatTime - 0, steptime:1});
    }
    var l = void 0;
    for(o = 0; o < r.length; o++)
    {
        0 < (T = r[o].arr).length && (void 0 === l || T.length < l) && (l = T.length);
    }
    for(o = 0; o < r.length; o++)
    {
        var i = r[o], T = i.arr;
        l && T.length > l && (T = T.slice(T.length - l)), l && 0 <= ",POWER_MY_WIN,POWER_BLOCKCHAIN,".indexOf("," + i.name + ",") && (T = SERVER.GetStatBlockchain(i.name,
        l));
        for(var g = 0, S = 0; S < T.length; S++)
            T[S] && (g += T[S]);
        0 < T.length && (g /= T.length);
        var _ = 1;
        if("MAX:" === i.name.substr(0, 4))
            for(; 500 <= T.length; )
                T = ResizeArrMax(T), _ *= 2;
        else
            for(; 500 <= T.length; )
                T = ResizeArrAvg(T), _ *= 2;
        i.AvgValue = g, i.steptime = _, i.arr = T.slice(1);
    }
    return r;
}, global.GET_STATS = function (e)
{
    var t = GetCurrentTime(), r = GetCurrentStatIndex();
    return {stats:{Counter:CONTEXT_STATS.Total, Counter10S:CalcInterval(CONTEXT_STATS, r, 10), Counter10M:CalcInterval(CONTEXT_STATS,
            r, 600)}, errors:{Counter:CONTEXT_ERRORS.Total, Counter10S:CalcInterval(CONTEXT_ERRORS, r, 10), Counter10M:CalcInterval(CONTEXT_ERRORS,
            r, 600)}, period:(t - StartStatTime) / 1e3, Confirmation:[]};
}, global.StartCommonStat = function ()
{
    for(var e in CONTEXT_STATS.Total)
        return ;
    ClearCommonStat();
}, global.ClearCommonStat = function ()
{
    StartStatTime = void (CurStatIndex = 0), CONTEXT_STATS = {Total:{}, Interval:[]}, CONTEXT_ERRORS = {Total:{}, Interval:[]},
    global.HASH_RATE = 0, SERVER.ClearStat();
}, global.ResizeArrAvg = ResizeArrAvg, global.ResizeArrMax = ResizeArrMax, DEBUG_MODE ? global.TO_DEBUG_LOG = function (e,t,r,o)
{
    DEBUG_MODE && ("rinfo" === t && (e += " from: " + r.address + ":" + r.port + " - " + o.length), ToLog(e));
} : global.TO_DEBUG_LOG = function (e,t,r,o)
{
}, global.GetStrOnlyTime = function (e)
{
    if(!global.GetCurrentTime)
        return ":::";
    e || (e = GetCurrentTime());
    var t = "" + e.getHours().toStringZ(2);
    return t = (t = (t = t + ":" + e.getMinutes().toStringZ(2)) + ":" + e.getSeconds().toStringZ(2)) + "." + e.getMilliseconds().toStringZ(3);
}, global.GetStrTime = function (e)
{
    if(!global.GetCurrentTime)
        return ":::";
    e || (e = GetCurrentTime());
    var t = "" + e.getDate().toStringZ(2);
    return t = (t = (t = (t = (t = (t = t + "." + (1 + e.getMonth()).toStringZ(2)) + "." + e.getFullYear()) + " " + e.getHours().toStringZ(2)) + ":" + e.getMinutes().toStringZ(2)) + ":" + e.getSeconds().toStringZ(2)) + "." + e.getMilliseconds().toStringZ(3);
};
