---
* [1. TERA ANN on BitcoinTalk](https://bitcointalk.org/index.php?topic=4573801.0)
* [2. TERA on Twitter](https://twitter.com/terafoundation)
* [3. TERA on Reddit](https://www.reddit.com/user/Terafoundation)
* [4. TERA on Discord](https://discord.gg/CvwrbeG)
* [5. TERA Telegram Chanel](https://web.telegram.org/#/im?p=@terafoundation)
* [6. TERA on QQ](https://jq.qq.com/?_wv=1027&k=5KpN5fw)

* [7. (German) Telegram ](https://t.me/terafoundation_germany)
* [8. (China) Telegram](https://t.me/TeraChina)

