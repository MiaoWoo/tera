/*
 * @project: TERA
 * @version: Development (beta)
 * @license: MIT (not for evil)
 * @copyright: Yuriy Ivanov (Vtools) 2017-2019 [progr76@gmail.com]
 * Web: https://terafoundation.org
 * Twitter: https://twitter.com/terafoundation
 * Telegram: https://web.telegram.org/#/im?p=@terafoundation
*/


function DoRest(r,t,e)
{
    var u = r.Arr[0], o = Math.floor(e / REST_BLOCK_SCALE);
    if(o !== Math.floor((u.BlockNum - 1) / REST_BLOCK_SCALE))
    {
        for(var R = GetRestArr(o), l = [], n = R.length - 2; 0 <= n; n--)
            l.push(R[n] * REST_BLOCK_SCALE);
        RestPush(r, l, e, 1);
    }
    r.Arr[0] = {BlockNum:e, Value:t.Value};
};

function RestPush(r,t,e,u)
{
    var o = r.Arr[u - 1], R = r.Arr[u];
    if(1 < u)
    {
        var l = t[u - 2];
        if(o.BlockNum > l)
            return ;
    }
    if(R.BlockNum && R.BlockNum >= e || o.BlockNum >= e)
        return R.BlockNum = 0, void (R.Value = {});
    R.BlockNum && u < r.Arr.length - 1 && RestPush(r, t, e, u + 1), r.Arr[u] = o;
};

function GetRestArr(r)
{
    for(var t = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], e = t.length, u = 0; u <= r; u++)
        for(var o = 0, R = u, l = e - 1; 0 <= l; l--)
        {
            var n = t[l];
            if(t[l] = R, R = n, 0 == ((o = o << 4 | 15) & u))
                break;
            if(0 != (o & R))
                break;
        }
    return t;
};
var RestArrMap = {};

function GetCurrentRestArr()
{
    var r = Math.floor(SERVER.BlockNumDB / REST_BLOCK_SCALE), t = RestArrMap[r];
    if(void 0 === t)
    {
        RestArrMap = {}, t = GetRestArr(r);
        for(var e = 0; e < t.length; e++)
            t[e] = t[e] * REST_BLOCK_SCALE;
        RestArrMap[r] = t;
    }
    return t;
};

function GetCurrentRestNum(r)
{
    for(var t = SERVER.BlockNumDB - r, e = GetCurrentRestArr(), u = e.length - 1; 0 <= u; u--)
        if(e[u] <= t)
            return e[u];
    return 0;
};
global.DoRest = DoRest, global.GetRestArr = GetRestArr, global.GetCurrentRestArr = GetCurrentRestArr, global.GetCurrentRestNum = GetCurrentRestNum;
