#### Protection from DDoS attacks

Each block has a limit of 130 KB; the average transaction size is 130 bytes, so an average of 1000 transactions per block is placed. Each transaction must have a PoW field, which records the work done to calculate the hash. This field is used to determine whether or not this transaction is included in the block. It must be at least a certain value and must be sufficient to compete with other transactions. Only the first (approximately) 1000 transactions with the highest PoW values are included in the block.
