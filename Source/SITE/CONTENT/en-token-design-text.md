The design of the coin symbolizes, on the one hand, the possibilities of technology to create a global economy without borders, on the other hand it demonstrates the principles of building a multi-level structured network in our blockchain.

You are free to use these images for your creative works and for spread information about our project around the world.