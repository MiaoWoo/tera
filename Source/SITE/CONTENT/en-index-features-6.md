#### TERA DApps


This is the next generation of smart contract development, any service can be implemented as a decentralized program that does not require a server for hosting (serverless technology). The original script of the program is not available for change by third parties and is free from censorship. They can work forever and belong to no one.