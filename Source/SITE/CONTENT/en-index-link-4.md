---
* [1. TERA Website](https://terafoundation.org/)
* [2. TERA EXPLORER](https://terafoundation.org/explorer.html)
* [3. TERA WALLET](https://terafoundation.org/web-wallet.html)
* [4. TERA NETWORK MAP](https://terafoundation.org/map.html)
* [5. TERA WHITE PAPER (EN)](https://terafoundation.org/files/TERA_WP_EN.pdf)
* [6. TERA WHITE PAPER (RU)](https://terafoundation.org/files/TERA_WP_RU.pdf)
* [7. TERA WHITE PAPER (DE)](https://terafoundation.org/files/TERA_WP_DE.pdf)
* [8. TERA WHITE PAPER (CN)](https://sourceforge.net/p/tera/code/ci/master/tree/Doc/Chinese/WP_chinese.pdf?format=raw)
