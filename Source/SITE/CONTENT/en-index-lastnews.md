## NEWS

**25.04.2019**

New article:
https://medium.com/@evkara777/decentralized-applications-on-tera-platform-2aa56b597ae9


**21.04.2019**


Today is another continuation update TeraNova:
1. In order to reduce the size of the database for new nodes, a new format of the database for storing block headers has been added (the functionality is disabled by default, activation will be performed in the next updates).
2.Changed the calculation of the hash of the transaction signature. To facilitate the integration of the Tera in other programming languages, starting with 25.5 million block hash signature will be taken by the sha3 algorithm.


The update is mandatory for all nodes.


**13.04.2019**


Now the Web wallet supports 5 languages: English, Chinese, Korean, German, Russian.
If you want to add a new language, download this [JSON file](https://terafoundation.org/files/lang.txt), perform the translation and place it on the telegram channel @TeraLab



**11.04.2019**


On April 9 and 10, 2019 TERA Foundation team took part in the conference on innovation, marketing and business "POTOK". The event was held in Russia, Moscow. https://potokconf.ru/
From now on, we will try to participate in all conferences that may be of interest to us.




**05.04.2019**


TeraNova - Step1

https://medium.com/@progr76/update-teranova-31dd3380f25c

**29.03.2019**

**The test network was updated to version 0.980**
Added the ability to load the blockchain chain from the end, a special constant is responsible for this. If the value is non-zero, the difference between the current blocks calculated based on the current time and the number recorded in the database is checked when the node is unsynchronized or restarted, if the difference is higher than the required value, the nearest table of residues is loaded from the network and the rest of the block chain is loaded from it.


By default, the value is set to 10000 blocks.
```js
"REST_START_COUNT": 10000
```

---
**26.03.2019**

### The solution to the problem of Blockchain Sharding
https://medium.com/@progr76/the-solution-to-the-problem-of-sharding-the-blockchain-ee90e001e22

---
**23.03.2019**



### We are starting to create DAO.


Please give feedback https://discord.gg/2dwydVh


---
**23.03.2019**

### The parametric diagram-calculator of the future new formula
https://terafoundation.org/emission.html

Params:

* Point 43 - start new formula
* Point 45 - approximating point
* KTera multiplier in the new formula of payment of awards


Formula: **One billionth of the remainder of undistributed amount of coins multiplied  by constant KTera**
```js
Reward=KTera*Account0/1000000000
```



---
**21.03.2019**


### New blockchain desynchronization detector test

* Note from  08.04.2019:  Now its work in main-net

The test network begins the test of a new detector for determining the desynchronization of the blockchain from the main chain.
If the last 10 blocks are created by their own node, then their POW should not differ more than 10 times from the previous blocks, otherwise the forced synchronization of the blockchain with the network is started.
Settings can be changed-new constant added:
```
  "RESYNC_CONDITION": {
    "OWN_BLOCKS": 10,
    "K_POW": 10
  },
```


