/*
 * @project: TERA
 * @version: Development (beta)
 * @license: MIT (not for evil)
 * @copyright: Yuriy Ivanov (Vtools) 2017-2019 [progr76@gmail.com]
 * Web: https://terafoundation.org
 * Twitter: https://twitter.com/terafoundation
 * Telegram:  https://t.me/terafoundation
*/


function DoRest(r,t,e)
{
    var u = r.Arr[0], o = Math.floor(e / REST_BLOCK_SCALE);
    if(o !== Math.floor((u.BlockNum - 1) / REST_BLOCK_SCALE))
    {
        for(var n = GetRestArr(o), l = [], a = n.length - 2; 0 <= a; a--)
            l.push(n[a] * REST_BLOCK_SCALE);
        RestPush(r, l, e, 1);
    }
    r.Arr[0] = {BlockNum:e, Value:t.Value};
};

function RestPush(r,t,e,u)
{
    var o = r.Arr[u - 1], n = r.Arr[u];
    if(1 < u)
    {
        var l = t[u - 2];
        if(o.BlockNum > l)
            return ;
    }
    if(n.BlockNum && n.BlockNum >= e || o.BlockNum >= e)
        return n.BlockNum = 0, void (n.Value = {});
    n.BlockNum && u < r.Arr.length - 1 && RestPush(r, t, e, u + 1), r.Arr[u] = o;
};

function GetRestArr(r)
{
    for(var t = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], e = t.length, u = 0; u <= r; u++)
        for(var o = 0, n = u, l = e - 1; 0 <= l; l--)
        {
            var a = t[l];
            if(t[l] = n, n = a, 0 == ((o = o << 4 | 15) & u))
                break;
            if(0 != (o & n))
                break;
        }
    return t;
};
var RestArrMap = {};

function GetCurrentRestArr()
{
    var r = GetCurrentBlockNumByTime(), t = Math.floor(r / REST_BLOCK_SCALE), e = RestArrMap[t];
    if(void 0 === e)
    {
        RestArrMap = {}, (e = GetRestArr(t)).length = e.length - 1;
        for(var u = 0; u < e.length; u++)
            e[u] = e[u] * REST_BLOCK_SCALE;
        RestArrMap[t] = e;
    }
    return e;
};

function GetCurrentRestNum(r)
{
    for(var t = GetCurrentBlockNumByTime() - r, e = GetCurrentRestArr(), u = e.length - 1; 0 <= u; u--)
        if(e[u] <= t)
            return e[u];
    return 0;
};
global.DoRest = DoRest, global.GetRestArr = GetRestArr, global.GetCurrentRestArr = GetCurrentRestArr, global.GetCurrentRestNum = GetCurrentRestNum;
