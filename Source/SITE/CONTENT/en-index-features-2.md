#### High Transaction Throughput

TERA blockchain is capable of throughput of 1000 transactions per second while retaining all praised blockchain features and bringing huge possibilities for further development and seamless integration with existing infrastructure as well as with new emerging technologies.