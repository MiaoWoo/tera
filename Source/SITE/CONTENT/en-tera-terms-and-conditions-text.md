1. SCOPE



These are the Terms of Use (hereinafter referred to as the “Terms”) of the TERA Foundation (hereinafter referred to as “TERA”, ”we” or “us”). These Terms apply to any access and use of TERA DEX, TERA DApp and TERA Wallet, the TERA cryptographic coin (hereinafter referred to as “TERA”), the TERA website at https://www.terafoundation.org/, the online services, and any TERA’s services related to or utilizing any of the foregoing which we refer to in these Terms, collectively, as “Services”, “TERA Services” or “our Services”.


2. ELIGIBILITY AND AGREEMENT


You must ensure that you use and access TERA Services only in your own name. If you are acting for a legal entity, you must ensure that you:
(a) use and access TERA Services on behalf of the legal entity; and
(b) that you are authorised to enter into transactions on behalf of the legal entity.


3. YOUR RESPONSIBILITIES REGARDING THE USE OF THE SERVICES


You are also responsible for maintaining adequate security, control and confidentiality of your device access, your TERA Wallets information, including passwords, passphrases, private keys or other codes associated with your TERA Wallets and any activity occurring within these TERA Wallets. The loss or compromise of this information may result in unauthorized access of your TERA Wallets, and loss or theft of any TERA and other cryptographic tokens held in your TERA Wallets.
If you believe your TERA Wallets have been compromised, or you need to report a security incident, or you have experienced any operational problems, or have a security concern, please contact us immediately at help@terafoundation.org describing the issue at hand as thoroughly as possible including the date, type of problem and part of the TERA site or TERA Services where you experienced that problem. You are responsible for (i) immediately notifying us of any unauthorized use of your password or TERA Wallets or any other breach of security, and (ii) ensuring that you log out from your TERA Wallets at the end of each session when accessing the TERA Services.
We have no responsibility for any loss that you suffer as a result of failing to comply with this section or failure to follow or act on any notices or alerts that we may send to you.


4. AVAILABILITY OF SERVICES


Subject to these Terms, TERA shall use reasonable efforts to make available, operate and maintain the TERA Services during the term of these Terms and to permit you to access and use the TERA Services in accordance with these Terms. TERA shall use all reasonable efforts to promptly notify you of any difficulties experienced by us or other participants with respect to their access to or the use of the TERA Services, but only to the extent that TERA is aware of such difficulties and reasonably determines that they are material to your access and use of the TERA Services. Similarly, you shall notify TERA the soonest possible in case you become aware of any material technical failures of or difficulties with the TERA Services or upon becoming aware of any material breach (or any event which, by giving notice and/or the lapse of time, would constitute a material breach) of these Terms.
Our Services may evolve over time. This means we may apply changes, replace, or discontinue (temporarily or permanently) our Services at any time for any reasonable cause with two days’ notice or without notice in case of a Force Majeure. In this case, you may be prevented from accessing or using our Services. If, in our sole discretion, we decide to permanently discontinue our Services, we will provide you with a notice via our website, via our Twitter account or any other means of communication we deem appropriate.
You accept and acknowledge that the TERA Services may not be accessible in every country of your residence, in particular because of regulatory requirements.


5. FORCE MAJEURE


A Force Majeure Event includes without limitation each of the following:
a) Government actions, the outbreak of war or hostilities, the threat of war, acts of terrorism, national emergency, riot, civil disturbance, sabotage, requisition, or any other international calamity, economic or political crisis;
b) Act of God, earthquake, tsunami, hurricane, typhoon, accident, storm, flood, fire, epidemic or other natural disaster;
c) Labour disputes and lock-out;
d) Breakdown, failure or malfunction of any electronic, network and communication lines or systems (not due to the fault of TERA);
e) Any event, act or circumstances not reasonably within TERA’s control and the effect of that event(s) is such that TERA is not in a position to take any reasonable action to cure the default.


6. RISKS


You understand and accept the risks in connection with the use of the TERA Wallet app and using the Services as set forth above and hereinafter. In particular, but not limited to, you understand the inherent risks listed hereinafter:
a) Risk of software weaknesses: You understand and accept that the underlying software application and software platform is still in an early development stage and unproven, why there is no warranty that the Services will be uninterrupted or error-free and why there is an inherent risk that the software could contain weaknesses, vulnerabilities or bugs causing, inter alia, the complete loss of TERA.
b) Regulatory risk: You understand and accept that blockchain technology allows new forms of interaction and that it is possible that certain jurisdictions will apply existing regulations on, or introduce new regulations addressing, blockchain technology based applications, which may be contrary to the current setup of TERA and which may, inter alia, result in substantial modifications of the TERA Services, including its termination.
c) Risk of loss of private key or passphrase(s): TERA Wallet can only be accessed by using a TERA Wallet passphrase with the possibility of using a second passphrase. You understand and accept that if your private key, passphrase or second passphrase respectively got lost or stolen, the TERA within your TERA Wallet will be unrecoverable and will be permanently lost.
d) Risk of voting attacks: You understand and accept that the blockchain used for by TERA is susceptible to voting attacks, including but not limited to majority voting power attacks, “selfish-voting” attacks, and race condition attacks. Any successful attacks present a risk to the TERA Services.
e) Risk of delegate attacks: You understand and accept that the blockchain used for by TERA is susceptible to delegate attacks, including but not limited to double-spending attacks, majority delegate attacks, and race condition attacks. Any successful attacks present a risk to the TERA Services.


7. PUBLIC AND PRIVATE KEY, PASSPHRASES


When you create a TERA Wallet, the Services generate a digital private and public key pair and a passphrase. The Services never store either private key or passphrase. The public key generated by the Services is being used to generate a TERA Wallet address, and may be shared with the network and with others to complete transactions. The private key is associated to a TERA Wallet address and must be used in conjunction with the TERA Wallet address to authorize transactions from or to that TERA address.
You need to make sure that your passphrase(s) are properly backed up and protected from theft.


8. DATA PROTECTION


The information provided pursuant to these Terms will be used by TERA for the purposes of providing you with services and data pursuant to these Terms and enabling TERA to perform its activities.
You acknowledge and agree that TERA may disclose your data, including personal data and sensitive personal data as defined under the Swiss Federal Data Protection Act (“Participant Data”) to outside organisations for the purpose of providing services and data to you, and performing its activities. You explicitly consent to the export of your data to a location outside your country of domicile and to third parties outside of TERA.


9. PROHIBITED ACTIVITIES


You agree that you will not use the TERA Services to perform any type or sort of illegal activity or to take any action that negatively affects the performances of the TERA Services. You may not engage via the Services in any of the following activities, nor help a third party in any such activity to:
1) attempt to gain unauthorized access to our Services or another user’s TERA Wallet;
2) make any attempt to bypass or circumvent any security features;
3) violate any law, statute, ordinance, regulation or these Terms and other contractual documents as referred to herein;
4) reproduce, duplicate, copy, sell or resell our Services for any purpose except as authorized in these Terms;


10. DEFAULT


Each of the following constitutes an “Event of Default”:
a) Where any representation or warranty made by you is or becomes untrue;
b) Any other circumstance where TERA reasonably believes that it is necessary or desirable to take any action set out in the below paragraph;
c) You are performing a prohibited activity as specified in section 9, you involve TERA in any type of fraud or illegality and if TERA suspects that you are engaged into money laundering activities or terrorist financing or other criminal activities;
d) Commencement of proceedings or investigations against you by a governmental authority, including but not limited to the request for an action set out in the below paragraph by a competent governmental authority or body or court;
e) In cases of material violation by you of the requirements established by any applicable laws, such materiality determined in good faith by TERA;
f) Any other situation where it would not be in the best interest of TERA that you continue to be a participant.
If an Event of Default occurs, TERA may at its absolute discretion, at any time and without prior notice, take one or more of the following actions:
a) Terminate these Terms without notice;
b) Close any or all of your TERA Wallets;
c) Refuse to open a new TERA Wallet for you.

