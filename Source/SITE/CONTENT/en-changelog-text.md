v. 0.897


* **v. 0.922:**
* 1) The new update fixed bugs of API 
* 2) Limited top reward value (from 22.5 million blocks)
* 3) Added new methods for API2

The API-2 is designed to make it easier to write third-party applications. Server-side cryptography and POW operations are performed. Therefore, it is not recommended for public access, because it is not protected from DDOS attacks. Use it if applications such as the exchange server are on the same private network.

* **v. 0.884**

Will be enhanced the behavior of the constants COREY_WATCH_DOG. If you will be the number 2: "COREY_WATCH_DOG": 2, instead of an overload will just trim the blocks on the 5000, it will save time filling out the memory hashes. I. e. mining will practically go without interruption

* **v. 0.880**


It contains API Protocol improvements for DApp.

v. 0.867
Made Protocol support light-wallet, fixed a bug in validate download chains.
The update is optional and can be skipped.

v. 0.811
If you set a constant:

"WATCHDOG_BADACCOUNT": 1

then "BAD ACCOUNT Has"error checking will be enabled. When accumulating them in the amount of 60, automatic cutting of the blockchain into 5000 blocks will be launched, which in theory leads to the removal of this error. But this is not accurate.

v. 0.783
Fix history and dapp event bugs.

v. 0.753
The update is partially completed:
1. Half of the nodes have completed the update
2. The second half performs the transaction overwrite procedure (it will take a long time). To speed up, you can download the DB folder from the torrent.

v. 0.727
Fix minor bugs.

v. 0.719
The update is desirable for the stability of the network, which consists of nodes from different regions of the world. The algorithm of correction of the current number of the current block-robust with respect to the time deviation.

v. 0.718
The consensus algorithm was returned to its original level (0.703). The experiments of the new consensus failed. I'll think.
Thanks for your patience

v. 0.703
Fix visual part of DApp.
Made less memory usage when you turn off the logging of statistics.
The update does not contain any changes in the blockchain communication Protocol and can be omitted.

v. 0.685
Update 685 contains two mining constants (on the CONFIG tab, the CONSTANTS button):

"COUNT_MINING_CPU": 0,
"SIZE_MINING_MEMORY": 0,

If set to COUNT_MINING_CPU that starts the specified number of processes of mining, you can set more or fewer than there are physical cores.

If the value SIZE_MINING_MEMORY is set, this value is distributed to all processes (the specified number is divided by the number of processes).

v. 0.672
Fixed a hash table calculation bug (OUR ACCOUNT).
From the number 10195000 the calculation of the block hash changes.

v. 0.671
The current update 0.671 contains an improved miner. The exchange Protocol has not changed. It is recommended only if you want to improve mining.

v. 0.666
Button added Auto-update to wallet (near button mining). If the update mode is turned off, but a new version is uploaded to the network, the button becomes orange and it is written in the title bar of the window via slash (Example: 0.661/663).
Added the ability to send money to any public address. In this case, a new account is created upon receipt. The first line of the payment purpose is used as the name. For the creation of the account will be charged a fee.
Added the possibility of unlimited creation of new accounts, but with a fee. Now the fee is 10 Tera, but as the rate of the Tera increases, it will decrease. The fee is paid to account 0. To do this, when creating a new account, use the "Add to Pay list"button. Clicking on it on the Send tab creates a payment transaction for 10 Tera with the order to create an account. It must be sent manually-by pressing the Send button.
Added smart contracts. More detail will be description will be here soon.

v. 0.555
Fix problem with pow process.
More hashrate.

v. 0.553
Fixed small bug with update version installation (previously installed always, even if auto-update is disabled).
With block number 7000000 introduced protection against DDOS attacks when creating new accounts. New accounts will be allowed to be created only in limited quantities. For an unlimited number in the future will be expanded standard transaction transfer of funds. We plan to add support for transferring money to the wallet address (public key) without specifying an account. In this case, a new account will be created automatically and a small fee will be charged for its creation (for DDOS protection). Ordinary transaction of transfer of funds stating the account will not require a fee.

v. 0.545
Full utilization of memory and CPU.

More hashrate.

v. 0.542
More memory optimization. More hashrate.

v. 0.501
Small bugs fix.

v. 0.517
A small change in security. For limited web access to the wallet, you can set the external ip address of the computer from which access is allowed in the constant "HTTP_IP_CONNECT".
Example:
"HTTP_IP_CONNECT": "111.222.220.230",
Note:
Access to the wallet via the local address 127.0.0.1 is always allowed.

v. 0.501
Small bugs fix.

v. 0.452
The power of the network is growing. Now its value 2^27.6 = 200Mh/s.

v. 0.450
Reduce traffic pushed.

v. 0.418
Candidate stable 2 pushed.

v. 0.366
Minor changes to the interface part update 366:

The utility section now has the following buttons:
1. Rewrite transactions
2. Truncate chain
3. Clear DataBase

The first two items require you to enter a parameter - the depth of the chain for processing in blocks starting from the current one. The last point - clears completely the database, without affecting the parameters of the wallet and server constants. Works the same as deleting the DATA / DB folder.

v. 0.214
For the network, it is very important that the time on all nodes is the same. Otherwise, the exchange of packets becomes impossible. With the help of special algorithms, it is synchronized automatically and the general direction is such that it is equal to the UTC time. If you are sure that your server has exact time synchronization, then you can disable automatic synchronization (but under your responsibility). This is done in constants, set the value: "AUTO_COORECT_TIME": 0

v. 0.171
The second version with spam protection is released. Available in auto-update mode.

v. 0.169
Ban list enable.

v. 0.169
Ban list enable.

v. 0.162
If you do not specify a password for the http remote access port, access is allowed only from the local address 127.0.0.1.

v. 0.156
Added automatic tracking of the processes involved in mining. If processes lose connection with the main process, they are destroyed.

v. 0.155
You can set http password during the program installation from command line, example: node set httpport:8080 password:123.