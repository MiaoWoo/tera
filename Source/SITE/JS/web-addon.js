const fs = require('fs');

global.BlogList=[];
global.BlogMap=[];

function ReloadBufer()
{
    global.BlogList=[];
    global.BlogMap=[];
    global.SendHTMLMap={};
}
setInterval(ReloadBufer,60*1000);

HostingCaller.TestAddon=function (Params,response,ArrPath)
{
    return "Test. Your params:"+JSON.stringify(Params)+"  Path:"+JSON.stringify(ArrPath);
}


HostingCaller.blog=function (Params,response,ArrPath)
{
    if(ArrPath[1]==="reload")
    {
        ReloadBufer();
        return "Reload OK";
    }


    var PathUploads="./SITE/blog/uploads/";


    if(!global.BlogList.length)//init
    {
        var ArrList=fs.readdirSync(PathUploads);
        for (var i=0;i<ArrList.length;i++)
        {
            var name=ArrList[i];
            if(name.substring(name.length-5)===".html")
            {
                var filename=PathUploads+name;
                var Data={name:name,filename:filename};
                if(!CheckPostData(Data))
                    continue;
                BlogList.push(Data);
            }
        }

        BlogList.sort(function (a,b)
        {
            return parseInt(b.id)-parseInt(a.id);
        });

    }

    var TemplPath="./SITE/blog/";

    if(ArrPath.length===1 || (ArrPath.length===2 && !ArrPath[1]))
    {

        //list
        //var StrHeader = String(fs.readFileSync(TemplPath+"post-header.html"));
        var StrHeader = GetFileHTMLWithParsing(TemplPath+"post-header.html");
        var StrAdd="";
        for(var i=0;i<BlogList.length;i++)
        {
            var Data=BlogList[i];
            if(!Data)
                continue;

            if(!CheckPostData(Data))
                continue;
            var Str=StrHeader;
            for(var key in Data)
            {
                var str_repl=new RegExp("\\{\\{"+key+"\\}\\}",'g');
                Str=Str.replace(str_repl,Data[key]);
            }
            StrAdd=StrAdd+"\n"+Str;
        }
        //var StrList = String(fs.readFileSync(TemplPath+"index.html"));
        var StrList = GetFileHTMLWithParsing(TemplPath+"index.html");
        var index=StrList.indexOf("{{List}}");
        if(index<=0)
        {
            ToLog("Error file format index.html - not found {{List}}");
            return 0;
        }
        StrList=StrList.substring(0,index)+StrAdd+StrList.substring(index+8);

        response.writeHead(200, { 'Content-Type': 'text/html'});
        response.end(StrList);
    }
    else
    if(ArrPath.length===2)
    {
        var Num=parseInt(ArrPath[1]);
        var Data=BlogMap[Num];
        if(!Data)
        {
            return "Blog not found1:"+Num;
        }
        if(!CheckPostData(Data,1))
            return "Blog not found2:"+Num;

        //var Str = String(fs.readFileSync(TemplPath+"post.html"));
        var Str = GetFileHTMLWithParsing(TemplPath+"post.html");
        for(var key in Data)
        {
            var str_repl=new RegExp("\\{\\{"+key+"\\}\\}",'g');
            Str=Str.replace(str_repl,Data[key]);
        }

        response.writeHead(200, { 'Content-Type': 'text/html'});
        response.end(Str);

    }
    else
    if(ArrPath.length===4 && ArrPath[1]==="uploads")
    {
        var name=ArrPath[2]+"/"+ArrPath[3];
        var Path=PathUploads+name;
        SendWebFile(response,Path,"");
    }
    else
    {
        return "Your params:"+JSON.stringify(Params)+"  Path:"+JSON.stringify(ArrPath);
    }



    // var Path="./SITE/blog/";
    // fs.readdir(Path, function (Err,files)
    // {
    //     if(Err)
    //     {
    //         response.end("Error: "+Err);
    //         return;
    //     }
    //
    //     response.end(JSON.stringify(files));
    // });



    return null;
}

function CheckPostData(Data, bBody)
{
    if(!Data.Init)
    {
        var Str = String(fs.readFileSync(Data.filename));
        if(!Str.length>0)
        {
            ToLog("Error file length: "+Data.filename);
            return 0;
        }

        var index=Str.indexOf("=START=");
        if(index<=0)
        {
            ToLog("Error file format: "+Data.filename);
            return 0;
        }

        var Str1=Str.substring(0,index);


        try
        {
            var Data2=JSON.parse(Str1);
        }
        catch(e)
        {
            ToLog("Error: "+e+"\n\n"+Str1)
            return 0;
        }

        for(var key in Data2)
        {
            Data[key]=Data2[key];
        }

        global.BlogMap[Data.id]=Data;

        if(bBody)
        {
            var Str2=Str.substring(index+7);
            Data.Body=Str2;
        }

        //Data.Init=1;
    }
    return 1;
}

//document.body.contentEditable='true'; document.designMode='on';


HostingCaller.GetFileContent=function (Params,response,ArrPath)
{
    if(!global.HTTP_ADMIN_PASSORD)
        return {result:0,Text:"Not set password"};
    if(Params.Password!==global.HTTP_ADMIN_PASSORD)
        return {result:0,Text:"Error password"};

    var Str=GetFileSimple(Params.Path);
    return {result:1,Body:Str};
}

HostingCaller.SaveFileContent=function (Params,response,ArrPath)
{
    if(!global.HTTP_ADMIN_PASSORD)
        return {result:0,Text:"Not set password"};
    if(Params.Password!==global.HTTP_ADMIN_PASSORD)
        return {result:0,Text:"Error password"};

    SaveFileSimple(Params.Path,Params.Body);
    var Str=GetFileHTMLFromMarkdown(Params.Path);
    return {result:1,Body:Str};
}

// HostingCaller.GetFileMarkdown=function (Params,response,ArrPath)
// {
//     if(!global.HTTP_ADMIN_PASSORD)
//         return {result:0,Text:"Not set password"};
//     if(Params.Password!==global.HTTP_ADMIN_PASSORD)
//         return {result:0,Text:"Error password"};
//
//     var Str=GetFileHTMLFromMarkdown(Params.Path);
//     return {result:1,Body:Str};
// }


