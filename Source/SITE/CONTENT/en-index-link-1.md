
* [1. TERA Source Code](https://gitlab.com/terafoundation/tera)
* [2. TERA DApp PAPER (ENG)](https://docs.google.com/document/d/1PXVBbMKdpsAKPkO9UNB5B-LMwIDjylWoHvAAzzrXjvU/edit?usp=sharing)
* [3. TERA DApp PAPER (RUS)](https://docs.google.com/document/d/1SkD4yc_POaGRMJRC6yGkDfdJUuKbcyq3JpG0cBXeYGM/edit?usp=sharing/)
* [4. FAQ](https://docs.google.com/document/d/10yXAKxaU7YgrQnbdXu_L7WWovUoRtdJwo3tXXaGZGSQ/edit?usp=sharing/)
* [5. TERA API](https://gitlab.com/terafoundation/tera/blob/master/Doc/Eng/API.md)
* [6. TERA API-2 (for Exchanges)](https://gitlab.com/terafoundation/tera/blob/master/Doc/Eng/API2.md)