# TERA BLOG

If you have written something important about TERA and think you deserve a reward, please contact US:
* Discord: https://discord.gg/CvwrbeG
* Telegram: [@PsyArcus](https://web.telegram.org/#/im?p=@PsyArcus)
* See more about reward: [TERA CONTENT FUND](https://terafoundation.org/tera-content-fund.html)

