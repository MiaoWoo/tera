#### Fast Block Generation

TERA is the fastest block generation blockchain in decentralized blockchains. (Meaning that POS and the like are not decentralized). With 1s block generation and 8s to confirmation, your transaction is near instant.